<?php
namespace Trinetus\SymfonyDatatables\DTO;

use Symfony\Component\Serializer\Annotation\Groups;

/**
 * DTO object for json response required by JS Datatables client.
 *
 * @author Jan Smatlik <jan@trinetus.com>
 */
class DatatablesResponseDTO {
    
    private const DEFAULT_RECORDS_COUNT = 20;
    
    /** 
     * @Groups({"datatablesDTO"})
     * @var array $data 
     */
    private $data;
    
    /** 
     * @Groups({"datatablesDTO"})
     * @var int $draw 
     */
    private $draw;
    
    /** 
     * @Groups({"datatablesDTO"})
     * @var int $recordsTotal 
     */
    private $recordsTotal;
    
    /**
     * @Groups({"datatablesDTO"}) 
     * @var int $recordsFiltered 
     */
    private $recordsFiltered;
    
    
    /**
     * 
     * @param array $data Array of filtered and paginated records to output
     * @param int $filteredRecords count of all filtered records (also those, not currently paginated)
     * @param int $totalRecords count of all records without any filters and pagination
     */
    public function __construct(?array $data, int $filteredRecords=0, int $totalRecords=0)
    {
        // if any of variable is empty, we need to define max/default value for both
        if(empty($totalRecords) || empty($filteredRecords))
        {
            $totalRecords = $filteredRecords = ($filteredRecords ?? ($totalRecords ?? static::DEFAULT_RECORDS_COUNT));
        }
        
        $this->data = $data;
        $this->draw = 1;
        $this->recordsFiltered = $filteredRecords;
        $this->recordsTotal = $totalRecords;
    }
    
    
    public function getData(): ?array
    {
        return $this->data;
    }
    
    public function getDraw(): int
    {
        return $this->draw;
    }
    
    public function getRecordsTotal(): int
    {
        return $this->recordsTotal;
    }
    
    public function getRecordsFiltered(): int
    {
        return $this->recordsFiltered;
    }
    
}
