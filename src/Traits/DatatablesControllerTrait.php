<?php
namespace Trinetus\SymfonyDatatables\Traits;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Doctrine\Common\Collections\Expr\CompositeExpression;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Trinetus\SymfonyDatatables\DTO\DatatablesResponseDTO;

/**
 * Symfony Controller trait for easier setup of Datatables backend response.
 *
 * @author Jan Smatlik <jan@trinetus.com>
 */
trait DatatablesControllerTrait {
    
    protected $request;
    protected $enableSerializerMaxDepth = false;
    protected $customJsonSerializer;
    protected $serializationGroups = [];
    
    
    public function setRequest(Request $request): void
    {
        $this->request = $request;
    }
    
    public function getFilterCriteria(): ?Criteria
    {
        $anyCriteria = false;
        $criteria = Criteria::create();
        
        foreach($this->getFilter() as $key=>$val)
        {
            if($val != null) 
            {
                $criteria->andWhere(new Comparison($key, Comparison::EQ, $val));
                $anyCriteria = true;
            }
        }
        
        return $anyCriteria ? $criteria : null;
    }
    
    public function getSearchCriteria(array $searchInColumns, ?Criteria $criteria): ?Criteria
    {
        $searchStr = $this->getSearch();
        
        if(empty($searchInColumns) || empty($searchStr))
            return $criteria;
            
        if(empty($criteria))
            $criteria = Criteria::create();
        
        
        $orxList = [];
        foreach($searchInColumns as $key)
        {
            $orxList[] = Criteria::expr()->contains($key, $searchStr);
        }

        $criteria->andWhere(new CompositeExpression(CompositeExpression::TYPE_OR, $orxList));

        return $criteria;
    }
    
    public function getPaginationCriteria(): Criteria
    {
        $criteria = Criteria::create()
                        ->setFirstResult($this->getOffset())
                        ->setMaxResults($this->getLimit());
        
        $orderBy = $this->getOrderBy();
        if(!empty($orderBy))
        {
            $criteria->orderBy($orderBy);
        }               
        
        return $criteria;
    }
    
    public function datatablesJson(?array $data, int $dataCount=0, int $responseCode=200): Response
    {
        $serializer = $this->customJsonSerializer ?? $this->getDefaultSerializer();
        $dtResponse = new DatatablesResponseDTO($data, $dataCount);
        
        $props = [ 
            ObjectNormalizer::ENABLE_MAX_DEPTH => $this->enableSerializerMaxDepth,
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ];
        
        if(!empty($this->serializationGroups))
        {
            $props[ObjectNormalizer::GROUPS] = array_merge(["datatablesDTO"], $this->serializationGroups);
        }
        
        return new Response($serializer->serialize($dtResponse, 'json', $props), $responseCode, [ "Content-type" => "application/json; charset=utf8" ]);
    }
    
    
    protected function getFilter(): array
    {
        $filterJsonArr = json_decode($this->request->query->get('filter'), true);
        return $filterJsonArr ?? [];
    }
    
    protected function getOrderBy(): array
    {
        return ($this->request->query->get('orderBy') ? [ $this->request->query->get('orderBy') => ($this->request->query->get('orderByDir') ?? "ASC") ] : null );
    }
    
    protected function getLimit(): int
    {
        return $this->request->query->get('limit') ?? 10;
    }
    
    protected function getOffset(): int
    {
        return $this->request->query->get('skip') ?? 0;
    }
    
    protected function getSearch(): string
    {
        return $this->request->query->get('search') ?? "";
    }
    
    protected function getDefaultSerializer(): Serializer
    {
        return new Serializer([ $this->getDefaultNormalizer() ], [ new JsonEncoder() ]);
    }
    
    protected function getDefaultNormalizer(): ObjectNormalizer
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        
        $normalizer = new ObjectNormalizer($classMetadataFactory);
        
        return $normalizer;
    }
}