# Symfony Datatables

`This is only test package. Please don't use in production!`

## Install:

```bash
composer require trinetus/symfony-datatables
```

## Usage:
- Use Controller trait:

```php
use Trinetus\DatatablesControllerTrait;
```

- Get filtering criteria and filter+paginate Doctrine related data and return as Json (datatables server-side compatible):

```php
public function list(Request $request)
{
    $this->setRequest($request);
    $criteria = $this->getFilterCriteria();
    $criteria = $this->getSearchCriteria(["title","description"], $criteria);
        
    $data = $this->getRepository(MyEntity::class)->findByCriteria($criteria);

    $totalRecords = $this->getRepository(Product::class)->countFromCollection($data);

    return $this->datatablesJson($data->addCriteria($this->getPaginationCriteria())->getQuery()->getResult(), $totalRecords);
}
```

- Example of custom implementation of Repository methods for getting data and total count of data:

```php
public function findByCriteria(?Criteria $filterCriteria): QueryBuilder
{
    $qb = $this->createQueryBuilder('p');

    if(!empty($filterCriteria))
    {
        $qb->addCriteria($filterCriteria);
    }

    return $qb;
}

public function countFromCollection(QueryBuilder $collection)
{
    return (clone $collection)->select('COUNT(p)')->getQuery()->getSingleScalarResult();
}
```
